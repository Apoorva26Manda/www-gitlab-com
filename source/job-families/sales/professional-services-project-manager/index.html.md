---
layout: job_family_page
title: "Professional Services Project Manager"
---

To learn more, see the [Professional Services Engineer handbook](/handbook/customer-success/professional-services-engineering)

## Summary

Manage high-complexity projects to successful completion from initiation through delivery to closure. The Gitlab Consulting team is looking for a Senior Project Manager to join the Professional Services team to work with our global customers. In this role, you will coordinate with cross-functional teams to complete distinct projects both on time and within budget. You'll need to have substantial experience managing IT and software consulting projects and relying on traditional systems and software development methodologies, as well as practical experience with agile project management methodologies.

## Responsibilities

- Develop and manage schedules to deliver technical and consulting services to GitLab customers
- Manage customer expectations, project scope, and resources needed to successfully deliver to customers’ needs
- Develop and communicate project, portfolio, and program status, risks and issues to all levels of stakeholders, ranging from technical staff to executives
- Manage customer escalations, coordinating with customers and GitLab stakeholders (e.g., executive team, sales, technical account managers, etc.)
- Be accountable for reliable and repeatable program delivery processes, metrics and operational procedures
- Act as a liaison to subcontractors and/or delivery partners
- Develop a foundational knowledge of Gitlab’s technologies
- Maintain an awareness of emerging Gitlab's technologies

## Requirements

- Knowledge of specific industry project management and technical delivery methodologies for software (e.g., Project Management Institute (PMI) methodologies, agile/scrum, and/or software SDLC)
- Bachelor's degree in engineering, computer science or related field
- 5+ years of experience working as a project manager on IT-based projects
- 3+ years experience managing projects with external customers or clients, preferably enterprise customers. Professional Services experience is a plus.
- Demonstrated ability to motivate the advisor team and individual contributors and mediate conflicts
- Experience using various project management and/or agile tools
- Understanding of software development lifecycle, preferably experience in an agile and/or DevOps environment
- Excellent customer-facing and internal communication skills
- Great written and verbal communication skills
- Solid organizational skills, including attention to detail and ability to handle multiple priorities
- Willingness to be present on customer sites and to travel up to 20%
- Demonstrated ability to think strategically about business, products, and technical challenges
- Ability to use GitLab

### Level

## Senior Professional Services Project Coordinator

## Responsibilities

* Collaborate with Professional Services leaders to provide reports focused on financial and resource management data to aid with revenue and budgetary goals.
* Provide analysis that supports the business objectives of the Services organization, in alignment with corporate vision
* Run weekly time reports to estimate revenue; identify any resource or revenue corrective actions that are required to meet/beat monthly target
* Run monthly/quarterly billing; work with accounting to record revenue.
* Develop analytical reports for all sources of services revenue, consistent with company standards, and management direction.
* Assist Services leadership team with resource management and proactive forecasting processes.
* Coordinate with Project Management Team and other groups within Sales / Operations to obtain relevant data about the project opportunity and staffing requirements 
* Subject matter expert for services personnel on all processes related to services revenue: time entry, project management processes relate to operations, customer billing, and staff utilization.
* Skills Matrix: Management of PS / Vertical Resource Skill Matrix, ensuring all PS resources have an updated Skills Matrix. 
* Continually provide suggestions and implement improvements to business and reporting processes that keep up with the changes in the organization.
* Support Professional Services staff to better understand revenue, hours, and target attainment goals
* Act as liaison between the Project Management Office, Finance department, and Services staff.
* Ensure timely and complete new PS employee onboarding with content tailored to the new hires’ positions

## Requirements

* Bachelor’s degree required, preferably in Finance, Accounting, Statistics, or Business Management
* 4-6 years industry experience in a Professional Services Operations or similar role for a high-tech product company
* 2+ years of PSA tools experience required (i.e. NetSuite Openair, FinancialForce)
* Previous experience in a data-driven, analytics-centric role
* Working understanding of Professional Services cost structures strongly preferred
* Strong Microsoft Excel skills including experience with pivot tables, analyzing and formatting data, use of filters, and creating charts and graphs
* Excellent communication and presentation skills
* Must have incredible attention to detail, be self-starting, and possess strong organizational skills
* You share our values, and work in accordance with those values.
* [Leadership at GitLab](/handbook/leadership/#director-group)
* Ability to use GitLab

## Performance Indicators

* [CSAT](/handbook/support/support-ops/#customer-satisfaction-survey-csat ) >8.0/10 and [Project Margin](/handbook/customer-success/professional-services-engineering/#long-term-profitability-targets ) > 20% for assigned projects

## Hiring Process

Candidates can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

- Qualified candidates for the Professional Services Project Manager will receive a short questionnaire.
- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with a member of our Recruiting team.
- Next, candidates will be invited to schedule a first interview with our Director of Customer Success.
- For the Professional Services Project Manager role, candidates will be invited to schedule an interview with a Customer Success peer and may be asked to participate in a demo of a live install of GitLab.
- For the Federal Professional Services Project Manager role, candidates will be invited to schedule interviews with members of the Customer Success team and our Federal Regional Sales Director.
- Candidates may be invited to schedule an interview with our VP of customer success.

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing/).
